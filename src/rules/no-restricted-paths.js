/* eslint-disable */
const path = require("path");
const resolve = require("eslint-module-utils/resolve").default;
const isStaticRequire = require("eslint-plugin-import/lib/core/staticRequire").default;

module.exports = {
  meta: {
    type: "problem",
    /*docs: {
        url: docsUrl("no-restricted-paths"),
    },*/

    schema: [
      {
        type: "object",
        properties: {
          zones: {
            type: "array",
            minItems: 1,
            items: {
              type: "object",
              properties: {
                target: { type: "string" }, // regex
                deny: { type: "string" }, // regex
                allow: {
                  type: "array",
                  items: { type: "string" /* regex */ },
                  uniqueItems: true,
                },
                message: { type: "string" },
              },
              additionalProperties: false,
            },
          },
          basePath: { type: "string" },
        },
        additionalProperties: false,
      },
    ],
  },

  create: function nozones(context) {
    const options = context.options[0] || {}
    const zones = options.zones || []
    const basePath = options.basePath || process.cwd()
    const absoluteBasePath = path.resolve(basePath);
    const currentFilename = context.getFilename();

    function pathMatch(subject, pattern) {
      // sestaveni absolutnich cest - ty potrebuji pro kontrolu a korekci
      const aSubjectPath = path.resolve(basePath, subject);

      // Tento algoritmus pouziva zjednoduseny matching pouze nad relativni casti
      // cesty. Proto nejsou urcite konfigurace podporovany. Konkretne se jedna o
      // cesty, ktere zacinaji ../, nebo jinym zpusobem "sahaji pod" basePath.
      // Protoze je neumim resolvovat, ale protoze mohou byt validni, povazuji je
      // za validni. To je treba pripad referencovani node_modules, pokud je
      // basePath jine nez prazdne
      if (!aSubjectPath.startsWith(absoluteBasePath)) {
        return;
      }

      // standardizovane cesty
      // (+ 1 odstranuje lomitko, ktere by jinak bylo na zacatku)
      // (replace slouzi k nahrazeni windowsich lomitek do linuxove podoby)
      const sSubject = aSubjectPath.substr(absoluteBasePath.length + 1).replace(/\\/g, "/");

      // matchujeme relativni casti pres regexp
      return new RegExp(pattern).exec(sSubject);
    }

    function applyReplacementGroups(source, groups) {
      for (let i = 0; i < groups.length; i++)
        source = source.replace(new RegExp("\\$" + i, "g"), groups[i]);
      return source;
    }

    // urceni zon, pro ktere muzeme aplikovat
    // - do informace o zone pridame targetMatch, coz je vysledek volani
    //   patchMatches nad lintovanym souborem
    const matchingZones = zones
      .map(zone => ({ ...zone, targetMatch: pathMatch(currentFilename, zone.target) }))
      .filter(zone => !!zone.targetMatch);

    function checkForRestrictedImportPath(importPath, node) {
      const importPathResolved = resolve(importPath, context);
      if (!importPathResolved)
        return

      // pro vsechny aplikovatelne zony
      matchingZones.forEach((zone) => {
        const allowedPaths = zone.allow || [];
        const zoneDeny = zone.deny || "./"; // kdyz neni nastavene deny, je zakazane vse

        // na retezec "deny" aplikujme vysledek matchovani z "target"
        // - vede k nahrazeni symbolu $0, $1, $2 atd. za hodnoty odpovidajici
        //   matchovane polozce z "target"
        const denySubstit = applyReplacementGroups(zoneDeny, zone.targetMatch);
        const isRestricted = !!pathMatch(importPathResolved, denySubstit);

        // pokud dany import neni omezen pravidlem "from", neni touto zonou
        // zakazny a je vse v poradku
        if (!isRestricted)
          return;

        // pokud je import zakazany, musime se jeste podivat do allow, ktere mohou
        // efekt negovat
        if (allowedPaths.some(zoneAllow => {
          const allowSubstit = applyReplacementGroups(zoneAllow, zone.targetMatch);
          const isAllowed = !!pathMatch(importPathResolved, allowSubstit);
          return isAllowed;
        }))
          return; // je povoleno

        context.report({
          node,
          message: `Unexpected path "{{importPath}}"${zone.message ? `: '${applyReplacementGroups(zone.message, zone.targetMatch)}'` : " imported in restricted zone"}.`,
          data: { importPath },
        })
      })
    }

    return {
      ImportDeclaration(node) {
        checkForRestrictedImportPath(node.source.value, node.source)
      },
      CallExpression(node) {
        if (isStaticRequire(node)) {
          const [firstArgument] = node.arguments

          checkForRestrictedImportPath(firstArgument.value, firstArgument)
        }
      },
    }
  },
}